# Restorers - Basic Prototype


This is a prototype of Restorers mobile game project built int [Construct2](https://www.scirra.com/construct2) HTML5 game engine free version.

![restorers-prototype.png](https://gitlab.com/survivorsteam/restorers-prototype/raw/master/ImagesPreview/restorers-prototype-screenshot.png)

### What is this repository for?

* Validates and define mechanics and dynamics among members of **SurvivorsTeam**
* Share code with our members to evolution and colaboration
* Realease downloads to teachers and another students tests and feedbacks!

### Getting Started

1) Clone this repository into a directory of your choice, using the command below:

```ssh
git clone git@gitlab.com:survivorsteam/restorers-prototype.git
```

2) Install the required plugins below on Construct2

* [json-v1.2](https://www.scirra.com/forum/plugin-json-import-export-generate-edit-inspect_t100042) -> For inventory system

> **Installation:** Extract the zip file and copy the ``json-v1.2`` folder to ``Construct 2/exporters/html5/plugins``

* [ParallaxBG-1.0](https://www.scirra.com/forum/behavior-parallax-background-for-side-scrolling-games_t165505) -> For mountains parallax in the same layer

> **Installation:** Extract the zip file and drag the ``parallaxbg_v1.0.c2addon`` to the Construct2 main window

3) Open **Construct2** software, click on ``File => Open`` and select the ``RestorersPrototype.caproj`` file

4) Click on ``Run layout`` to show a preview playlable test

5) Enjoy :)
